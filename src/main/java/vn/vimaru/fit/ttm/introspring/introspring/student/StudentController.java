package vn.vimaru.fit.ttm.introspring.introspring.student;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
  /**
   * Trong xây dựng Rest API có 4 loại request HTTP thường dùng GET : Đùng để đọc
   * dữ liệu POST : Đùng để tạo hoặc cập nhật PUT : Dùng để cập nhật hoặc tạo
   * DELETE : Dùng để xóa Phần đường dẫn (path) trong URL được xử lý ở server gọi
   * là quá trình routing. Trong Spring sử dụng chỉ dẫn (Annotation) sau để thực
   * hiện thao tác này
   * 
   * @RequestMapping(path) @PostMapping
   * @GetMapping
   * @DeleteMapping
   * @PutMapping
   */

  @Autowired
  StudentService studentService;

  @RequestMapping(method = RequestMethod.GET, value = "/student/{msv}")
  public Optional<Student> getStudentInfo(@PathVariable String msv) {
    return studentService.getStudent(msv);
  }

  @RequestMapping(value="/student/{msv}", method=RequestMethod.PUT)
  public void updateStudent(@PathVariable String msv, @RequestBody Student st) {
      studentService.updateStudent(msv, st);
  }

  @RequestMapping(value = "/student/{msv}", method = RequestMethod.POST)
  public void createStudent(@PathVariable String msv, @RequestBody Student st) {
    studentService.createStudent(msv, st);
  }

  @RequestMapping(value = "/student", method = RequestMethod.GET)
  public List<Student> getAllStudents() {
    return studentService.getAllStudents();
  }

  @RequestMapping(value = "/student/search", method = RequestMethod.GET)
  public List<Student> searchByName(@RequestParam String name) {
    return studentService.findStudentByName(name);
  }

  @RequestMapping(value = "/student/advance_search", method = RequestMethod.GET)
  public List<Student> searchByNameOrMsv(@RequestParam String q) {
    return studentService.findStudentByNameOrMsv(q);
  }
}
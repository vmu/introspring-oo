package vn.vimaru.fit.ttm.introspring.introspring.student;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
  private String name;

  @Id
  private String msv;
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMsv() {
    return msv;
  }

  public void setMsv(String msv) {
    this.msv = msv;
  }

  public Student(String name, String msv) {
    this.name = name;
    this.msv = msv;
  }

  public Student() {
    name = "Student name";
    msv = "20191234";
  } 
}